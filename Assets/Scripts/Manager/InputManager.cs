﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Utility;
using System;

public class InputManager : Singleton<InputManager> {

    [SerializeField]
    private KeyCode actionKey = KeyCode.Space;
	
	void Update () {
	    if (OnTouchTap() || OnActionKey())
        {
            Spawner.Instance.Spawn();
        }
	}

    private bool OnActionKey()
    {
        return Input.GetKeyDown(actionKey);
    }

    private bool OnTouchTap()
    {
        return Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began; 
    }
}
